# Knative on K3S cluster

## How to install

Clone this project. Then:

- update `./vm.config`
- create the K3S cluster: `./create-vm.sh`
- install Knative **Serving**: `./knative-serving.sh`
- configure DNS (to use [xip.io](http://xip.io/)): `./knative-dns.sh`
- install Knative **Eventing**: `./knative-eventing.sh`

That's all.

## How to deploy applications

🚧 WIP

## K9S

Install K9S: [https://github.com/derailed/k9s](https://github.com/derailed/k9s)

## How to run K9S

```bash
export KUBECONFIG=$PWD/config/k3s.yaml
k9s --all-namespaces
```
