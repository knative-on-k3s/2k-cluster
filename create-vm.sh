#!/bin/sh
eval $(cat vm.config)

multipass launch --name ${vm_name} --cpus ${vm_cpus} --mem ${vm_mem} --disk ${vm_disk} \
	--cloud-init ./cloud-init.yaml

# Initialize K3s on node
echo "👋 Initialize 📦 K3s on ${vm_name}..."

multipass mount config ${vm_name}:config

# install k3s
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
curl -sfL https://get.k3s.io | sh -s - --disable traefik

EOF

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

echo "😃 📦 K3s initialized on ${vm_name} ✅"
echo "🖥 IP: ${IP}"

multipass exec ${vm_name} sudo cat /etc/rancher/k3s/k3s.yaml > config/k3s.yaml
sed -i '' "s/127.0.0.1/$IP/" config/k3s.yaml

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF

kubectl wait --for=condition=available deployment/coredns -n kube-system 
kubectl wait --for=condition=available deployment/local-path-provisioner -n kube-system  
kubectl wait --for=condition=available deployment/metrics-server -n kube-system  

echo "🎉 K3S installation complete 🍾"
EOF